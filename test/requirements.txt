pytest==6.2.2
pytest-ordering==0.6
flake8==3.7.7

bitbucket_pipes_toolkit==2.2.0
